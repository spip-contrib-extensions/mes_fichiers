<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_archive_mf_creer_charger_dist() {

	// On charge la liste de tous fichiers/dossiers pouvant être archivés sans filtrer par la taille max
	include_spip('inc/mes_fichiers');
	$liste = mes_fichiers_lister();

	// Taille max des fichiers/dossiers autorisés
	$megaoctets_max = intval(lire_config('mes_fichiers/taille_max_rep', '75'));
	// Convertir la taille max de Mo en octets pour comparaison
	$octets_max = taille_convertir_en_octets($megaoctets_max);

	// On prépare la liste des fichiers/dossiers pour un affichage simple dans le formulaire
	include_spip('mes_fichiers_fonctions');
	$chemins = [];
	foreach ($liste as $_chemin) {
		// Calcul de la taille des fichiers/dossiers
		$octets = mes_fichiers_lire_taille($_chemin);
		$chemins[$_chemin]['taille'] = mes_fichiers_afficher_taille($octets);
		$chemins[$_chemin]['disable'] = ($octets_max > 0) && ($octets > $octets_max);
	}

	return [
		'_fichiers'  => $chemins,
		'limite_max' => mes_fichiers_afficher_taille($octets_max),
		'editable'   => autoriser('sauvegarder')
	];
}

function formulaires_archive_mf_creer_verifier_dist() {

	// Initialisation des erreurs de saisie
	$erreurs = [];

	// Il devient obligatoire de saisir une liste non vide à archiver
	if (!_request('a_sauver')) {
		$erreurs['a_sauver'] = _T('info_obligatoire');
	}

	return $erreurs;
}

function formulaires_archive_mf_creer_traiter_dist() {

	// Récupération de la liste des fichiers à archiver
	$chemins = _request('a_sauver');

	// Lancement de l'archivage
	include_spip('inc/mes_fichiers_archive_mf');
	$erreur = archive_mf_creer($chemins);

	if ($erreur) {
		$retour['message_erreur'] = _T('mes_fichiers:message_sauvegarde_nok', ['erreur' => $erreur]);
	} else {
		$retour['message_ok'] = _T('mes_fichiers:message_sauvegarde_ok');
	}

	return $retour;
}
