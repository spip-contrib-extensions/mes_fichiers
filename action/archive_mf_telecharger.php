<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
function action_archive_mf_telecharger() {

	// Securisation de l'argument nom de l'archive
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$chemin_archive = $securiser_action();

	// Si l'archive n'est pas accessible on renvoie une erreur
	if (!@is_readable($chemin_archive)) {
		spip_log("L'archive ${chemin_archive} n'est pas accessible pour le téléchargement", 'mes_fichiers' . _LOG_ERREUR);
		redirige_par_entete(generer_url_ecrire('mes_fichiers', 'etat=nok_tele', true));
	}

	// Autorisation
	if (!autoriser('webmestre')) {
		include_spip('inc/minipres');
		echo minipres();
		exit;
	}

	// Vider tous les tampons pour ne pas provoquer de Fatal memory exhausted
	$level = @ob_get_level();
	while ($level--) {
		@ob_end_clean();
	}

	// Telechargement du fichier d'archive
	header('Content-Description: File Transfer');
	header('Content-type: application/octet-stream');
	header('Content-Disposition: attachment; filename="' . basename($chemin_archive) . '"');
	header('Content-Transfer-Encoding: binary');
	header('Pragma: public');
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header('Content-Length: ' . filesize($chemin_archive));
	readfile($chemin_archive);
}
