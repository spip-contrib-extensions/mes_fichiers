<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
function action_archive_mf_supprimer() {

	// Securisation de l'argument nom de l'archive
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$chemin_archive = $securiser_action();

	// Si l'archive n'est pas accessible on renvoie une erreur
	if (!@is_readable($chemin_archive)) {
		spip_log("L'archive ${chemin_archive} n'est pas accessible pour le téléchargement", 'mes_fichiers' . _LOG_ERREUR);
		redirige_par_entete(generer_url_ecrire('mes_fichiers', 'etat=nok_supp', true));
	}

	// Autorisation
	if (!autoriser('webmestre')) {
		include_spip('inc/minipres');
		echo minipres();
		exit;
	}

	include_spip('inc/flock');
	supprimer_fichier($chemin_archive);
}
