<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Renvoie une taille de dossier ou de fichier humainement lisible en ajustant le format et l'unité.
 * 
 * @param int    $octets  Taille d'un dossier ou fichier en octets
 * @param string $systeme Système d'unité dans lequel calculer et afficher la taille lisble. Vaut `BI` (défaut) ou `SI`.
 *
 * @return string Taille affichée de manière humainement lisible
 */
function mes_fichiers_afficher_taille($octets, $systeme = 'BI') {

	// Texte à afficher pour la taille
	$affichage = '';

	static $unites = ['octets', 'ko', 'mo', 'go'];
	static $precisions = [0, 1, 1, 2];

	if ($octets >= 1) {
		// Déterminer le nombre d'octets représentant le kilo en fonction du système choisi
		$systeme = strtolower($systeme);
		if ($systeme === 'bi') {
			$kilo = 1024;
			$suffixe_item = "_$systeme";
			$module = 'mes_fichiers';
		} else {
			$kilo = 1000;
			$suffixe_item = '';
			$module = 'spip';
		}

		// Identification de la puissance en "kilo" correspondant à l'unité la plus appropriée
		$puissance = floor(log($octets, $kilo));

		// Calcul de la taille et choix de l'unité
		$affichage = _T(
			$module . ':taille_' . $unites[$puissance] . $suffixe_item,
			[
				'taille' => round($octets / pow($kilo, $puissance), $precisions[$puissance])
			]
		);
	}

	return $affichage;
}

/**
 * Compile la balise `#ARCHIVE_MF_LISTER` qui fournit la liste archives disponibles et accessibles.
 * La signature de la balise est : `#ARCHIVE_MF_LISTER`.
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_ARCHIVE_MF_LISTER_dist($p) {

	// Aucun argument à la balise.
	$p->code = "calculer_liste_archives_mf()";

	return $p;
}

/**
 * Permet l'appel de la fonction archive_mf_lister() pour le compte de la balise du même nom.
 *
 * @internal
 *
 * @return array
 */
function calculer_liste_archives_mf() {

	include_spip('inc/mes_fichiers_archive_mf');
	return archive_mf_lister();
}

/**
 * Compile la balise `#ARCHIVE_MF_AFFICHER_RESUME` qui détaille le contenu d'une archive et le présente sous
 * une forme HTML.
 * La signature de la balise est : `#ARCHIVE_MF_AFFICHER_RESUME{chemin_archive}`.
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_ARCHIVE_MF_AFFICHER_RESUME_dist($p) {

	$chemin = interprete_argument_balise(1, $p);
	$chemin = str_replace('\'', '"', $chemin);
	$p->code = "calculer_resume_archive_mf($chemin)";

	return $p;
}
/**
 * Permet l'appel de la fonction archive_mf_lister() pour le compte de la balise du même nom.
 *
 * @internal
 *
 * @param string $chemin
 *
 * @return string
 */
function calculer_resume_archive_mf($chemin) {

	include_spip('inc/mes_fichiers_archive_mf');
	return archive_mf_informer($chemin);
}
