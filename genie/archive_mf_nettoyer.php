<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
/**
 * Nettoyage journalier des archives obsolètes.
 *
 * @param timestamp $last
 */
function genie_archive_mf_nettoyer_dist($last) {

	// On supprime les archives obsolètes en fonction de la duree de conservation.
	include_spip('inc/mes_fichiers_archive_mf');
	archive_mf_nettoyer(['auteur' => 'cron']);

	return 1;
}
