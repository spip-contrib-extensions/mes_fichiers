<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
/**
 * Activation des sauvegardes regulieres si celles-ci ont ete activees
 * Par défaut une fois par jour (peut être modifié dans la conf)
 * Activiation du nettoyage journalier si activé.
 *
 * @param array $taches_generales
 */
function mes_fichiers_taches_generales_cron($taches_generales) {

	include_spip('inc/config');
	if (lire_config('mes_fichiers/sauvegarde_reguliere') === 'oui') {
		// Archivage journalière de tous les fichiers possibles
		$jour = lire_config('mes_fichiers/frequence', 1);
		$taches_generales['archive_mf_generer'] = $jour * 24 * 3600;
	}

	if (lire_config('mes_fichiers/nettoyage_journalier', 'oui') === 'oui') {
		// Nettoyage journalier des archives obsolètes
		$taches_generales['archive_mf_nettoyer'] = 24 * 3600;
	}

	return $taches_generales;
}
