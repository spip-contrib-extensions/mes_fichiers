<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Génération d'une archive périodique complète par le cron.
 *
 * @param  $last
 *
 * @return int
 */
function genie_archive_mf_generer_dist($last) {

	// On lance la sauvegarde périodique de tous les fichiers possibles avec comme auteur le CRON
	include_spip('inc/mes_fichiers_archive_mf');
	archive_mf_creer(null, ['auteur' => 'cron']);

	return 1;
}
