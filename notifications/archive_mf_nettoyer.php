<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
function notifications_archive_mf_nettoyer_dist($quoi, $id, $options) {

	if (
		include_spip('inc/config')
		and (lire_config('mes_fichiers/notif_active') === 'oui')
		and is_array($options['liste'])
		and !empty($options['liste'])
	) {
		// preparation de la liste des destinataires
		$preparer_destinataires = charger_fonction('mes_fichiers_destinataires', 'inc');
		$destinataires = $preparer_destinataires($quoi, $id, $options);

		// Construction du sujet du mail
		include_spip('inc/texte');
		$sujet_mail = '[' . typo($GLOBALS['meta']['nom_site'])
					. '][mes_fichiers] '
					. _T('mes_fichiers:message_cleaner_sujet');

		// Construction du texte du mail
		$duree = lire_config('mes_fichiers/duree_sauvegarde', 15);
		$liste_fichiers = "\n\r";
		foreach ($options['liste'] as $_fichier) {
			$liste_fichiers .= "- ${_fichier}\n\r";
		}
		$msg_mail = _T('mes_fichiers:message_notif_cleaner_intro', array('duree' => $duree)) . $liste_fichiers;

		// Envoi de la notification
		include_spip('inc/notifications');
		notifications_envoyer_mails($destinataires, $msg_mail, $sujet_mail);
	}
}
