<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
function notifications_archive_mf_creer_dist($quoi, $id, $options) {

	if (
		include_spip('inc/config')
		and (lire_config('mes_fichiers/notif_active') === 'oui')
		and !$options['err']
	) {
		// preparation de la liste des destinataires
		$preparer_destinataires = charger_fonction('mes_fichiers_destinataires', 'inc');
		$destinataires = $preparer_destinataires($quoi, $id, $options);

		// Determination de l'auteur de la sauvegarde
		if (intval($options['auteur'])) {
			$auteur = sql_getfetsel('nom', 'spip_auteurs', 'id_auteur=' . intval($options['auteur']));
		} else {
			$auteur = $options['auteur'];
		}

		// Construction du sujet du mail
		include_spip('inc/texte');
		$sujet_mail = '[' . typo($GLOBALS['meta']['nom_site'])
					. '][mes_fichiers] '
					. _T('mes_fichiers:message_sauver_sujet');

		// Construction du texte du mail
		$msg_mail = _T('mes_fichiers:message_notif_sauver_intro', array('auteur' => $auteur));

		// Envoi de la notification
		include_spip('inc/notifications');
		notifications_envoyer_mails($destinataires, $msg_mail, $sujet_mail);
	}
}
