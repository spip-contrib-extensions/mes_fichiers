<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/mes_fichiers?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// B
	'bouton_mes_fichiers' => 'Backup my files',
	'bouton_sauver' => 'Backup',
	'bouton_tout_cocher' => 'Check all',
	'bouton_tout_decocher' => 'Uncheck all',
	'bouton_voir' => 'Display',
	'bulle_bouton_voir' => 'Display archive content',

	// C
	'colonne_nom' => 'Filename',

	// E
	'erreur_aucun_fichier_sauver' => 'No file to backup',
	'erreur_repertoire_trop_grand' => 'This folder exceeds @taille_max@ MB and won’t be saved.',
	'explication_cfg_duree_sauvegarde' => 'Enter backup retention time (in days)',
	'explication_cfg_frequence' => 'Enter backup frequency (in days)',
	'explication_cfg_notif_mail' => 'Enter addresses separated by commas ",". These addresses are added to the webmaster’s address.',

	// I
	'info_liste_a_sauver' => 'Files and folders ready for backup:',
	'info_nettoyer' => 'Automatic cleaning is enabled (retention time in days: @conservation@).',
	'info_sauver_1' => 'This option creates an archive file containing site customization data such as the last database backup dump, template folders, image folder...',
	'info_sauver_2' => 'The archive file is created in <em>tmp/mes_fichiers/</em> and named <em>@prefixe@_aaaammjj_hhmmss.zip</em>.',
	'info_sauver_3' => 'Automatic backup is enabled (frequency in days: @frequence@).',

	// L
	'label_cfg_nettoyage_journalier' => 'Enable daily archive cleanup',
	'label_cfg_notif_active' => 'Enable backup and cleanup notifications',
	'label_cfg_prefixe' => 'Archive filename prefix',
	'label_cfg_sauvegarde_reguliere' => 'Enable periodic backup',
	'label_cfg_taille_max_rep' => 'Maximum size of files or folders to be backed up (Mio)',
	'legende_cfg_generale' => 'Common backup parameters',
	'legende_cfg_notification' => 'Notifications',
	'legende_cfg_sauvegarde_reguliere' => 'Automatic actions',

	// M
	'message_cleaner_sujet' => 'Cleaning up backups',
	'message_notif_cleaner_intro' => 'Automatic deletion of obsolete backups (creation date former than @duree@ days) was performed successfully. The following files have been deleted : ',
	'message_notif_sauver_intro' => 'New backup is available. It was created by @auteur@.',
	'message_rien_a_sauver' => 'No files or folders to backup.',
	'message_rien_a_telecharger' => 'No backups available for download.',
	'message_sauvegarde_nok' => 'Error during backup. The archive file was not created (@erreur@).',
	'message_sauvegarde_ok' => 'The archive file has been created.',
	'message_sauver_sujet' => 'Backup',
	'message_suppression_nok' => 'Error deleting archive.',
	'message_telechargement_nok' => 'Error downloading archive.',
	'message_zip_propriete_nok' => 'No properties available on this archive.',

	// R
	'resume_zip_auteur' => 'Created by',
	'resume_zip_compteur' => 'Backuped files & folders',
	'resume_zip_contenu' => 'Content summary',

	// T
	'taille_go_bi' => '@taille@ Gio',
	'taille_ko_bi' => '@taille@ Kio',
	'taille_mo_bi' => '@taille@ Mio',
	'taille_octets_bi' => '@taille@ bytes',
	'titre_boite_sauver' => 'Create a backup',
	'titre_boite_telecharger' => 'List of backups available for download',
	'titre_page_configurer' => 'Plugin configuration',
	'titre_page_mes_fichiers' => 'Backup my customisation files',
];
