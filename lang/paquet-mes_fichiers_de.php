<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-mes_fichiers?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// M
	'mes_fichiers_description' => 'Dieses Plugin produziert eine Archivdatei mit den individuellen Einstellungen, dem neuesten Datenbankbackup, den eigenen Skeletten, dem Verzeichnis für Medien und Dokumente ...
Das Archiv wird im Verzeichnis <code>tmp/mes_fichiers/</code> angelegt. Sein Dateiname enthält das Speicherdatum.
Die Archivierung kann regelmässig automatisch erfolgen. Überflüssige Archive werden gelöscht und Benachrichtigungen über alle Aktionen versandt.',
	'mes_fichiers_nom' => 'Meine Dateien',
	'mes_fichiers_slogan' => 'Eigene Dateien und Einstellungen archivieren',
];
