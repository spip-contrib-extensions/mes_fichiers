<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-mes_fichiers?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// M
	'mes_fichiers_description' => 'This plugin creates an archive file containing site customization data, such as the last database backup dump, custom template folders, image folder...
The archive file is created in <code>tmp/mes_fichiers/</code>.

It is also possible to run a regular automatic backup of all customization files combined with a cleanup of obsolete archives, and to notify all actions.',
	'mes_fichiers_nom' => 'My files',
	'mes_fichiers_slogan' => 'Backup your customisation files and folders',
];
