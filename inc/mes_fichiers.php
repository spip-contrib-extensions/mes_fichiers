<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Renvoie la liste des fichiers et repertoires pouvant être sauvegarder, en filtrant ou pas selon une taille
 * maximale.
 * 
 * @api
 *
 * @uses dossier_trouver_fichier_recent()
 * @uses mes_fichiers_filtrer()
 * 
 * @param bool $filtrer_max_octets Indique si la liste doit être filtrée en fonction d'une taille maximale
 *                                 (configurée).
 *
 * @return array Liste des fichiers et repertoires pouvant être sauvegarder
 */
function mes_fichiers_lister($filtrer_max_octets = false) {

	// Constituer la liste des fichiers et dossiers pouvant être sauvegardés
	// -- on garde en statique la liste complète non filtrée
	static $liste = array();

	if (!$liste) {
		// Dans le cas de la mutu, les fichiers de personnalisation sont places
		// - soit dans le site d'un sous-domaine, il faut donc aller les chercher a la racine de ce sous-domaine
		//   en utilisant _DIR_SITE
		// - soit dans le site principal, il faut dans ce cas utiliser _DIR_RACINE
		$dir_site = defined('_DIR_SITE')
			? _DIR_SITE
			: _DIR_RACINE;

		// Rechercher le fichier sqlite le plus récent depuis config/bases
		$tmp_db = defined('_DIR_DB') ? _DIR_DB : $dir_site . 'config/bases/';
		if ($fichier_db =  dossier_trouver_fichier_recent($tmp_db)) {
			$liste[] = $fichier_db;
		}

		// le fichier d'options si il existe
		if (
			@is_readable($f = $dir_site . _NOM_PERMANENTS_INACCESSIBLES . _NOM_CONFIG . '.php')
			or (!defined('_DIR_SITE') && @is_readable($f = _FILE_OPTIONS))
		) {
			$liste[] = $f;
		}

		// le fichier .htaccess a la racine qui peut contenir des persos
		$htaccess = defined('_ACCESS_FILE_NAME') ? _DIR_RACINE . _ACCESS_FILE_NAME : _DIR_RACINE . '.htaccess';
		if (@is_readable($htaccess)) {
			$liste[] = $htaccess;
		}

		// le fameux repertoire des documents et images
		$IMG = defined('_DIR_IMG') ? _DIR_IMG : $dir_site . 'IMG/';
		if (@is_dir($IMG)) {
			$liste[] = $IMG;
		}

		// le(s) dossier(s) des squelettes nommes
		if (strlen($GLOBALS['dossier_squelettes'])) {
			// Dans le cas d'une mutu, la globale est toujours mise à jour sous la forme sites/domaine/squelettes
			// Cela revient donc au même que pour une install non mutualisée, il faut donc utiliser _DIR_RACINE
			foreach (explode(':', $GLOBALS['dossier_squelettes']) as $_dir) {
				$dir = ($_dir[0] == '/' ? '' : _DIR_RACINE) . $_dir . '/';
				if (@is_dir($dir)) {
					$liste[] = $dir;
				}
			}
		} elseif (@is_dir($dir_site . 'squelettes/')) {
			$liste[] = $dir_site . 'squelettes/';
		}

		// le dernier fichier de dump de la base
		$tmp_dump = defined('_DIR_DUMP')
			? _DIR_DUMP
			: $dir_site . 'tmp/dump/';
		if ($fichier_dump =  dossier_trouver_fichier_recent($tmp_dump)) {
			$liste[] = $fichier_dump;
		}

		// On ajoute via un pipeline pour déclarer des fichiers à sauvegarder provenant de plugins
		// et on renvoie la liste complète sans doublons.
		$liste_en_plus = pipeline('mes_fichiers_a_sauver', []);
		$liste = array_unique(array_merge($liste, $liste_en_plus));
	}

	// Filtrage de la liste si demandé
	if ($filtrer_max_octets) {
		$liste = mes_fichiers_filtrer($liste);
	}

	return $liste;
}

/**
 * Filtre la liste des fichiers et dossiers à archiver selon la taille maximale configurée.
 * Si aucune limite n'est précisée en configuration, la liste n'est pas filtrée.
 * 
 * @api
 * 
 * @uses taille_convertir_en_octets()
 * @uses mes_fichiers_lire_taille()
 *
 * @param array $liste Liste des fichiers et repertoires pouvant être sauvegarder quelque soit leur taille
 *
 * @return array Liste des fichiers/dossiers filtrée selon la taille
 */
function mes_fichiers_filtrer($liste) {

	// Extraction de la taille max en mega octets et vérification de la nécessité de filtrer la liste
	include_spip('inc/config');
	$megaoctets_max = intval(lire_config('mes_fichiers/taille_max_rep', '75'));
	if ($megaoctets_max > 0) {
		// Convertir la taille max de Mo en octets pour comparaison
		$octets_max = taille_convertir_en_octets($megaoctets_max);

		// Exclusion des fichiers ou dossiers trop volumineux
		foreach ($liste as $_cle => $_chemin) {
			// Calcul de la taille du fichier ou dossier
			$taille_octets = mes_fichiers_lire_taille($_chemin);
			if ($taille_octets > $octets_max) {
				unset($liste[$_cle]);
			}
		}
	}

	return $liste;
}

/**
 * Renvoie la taille d'un fichier ou d'un répertoire exprimée en octets.
 * 
 * @uses dossier_calculer_taille()
 *
 * @param string $chemin Chemin vers le fichier ou le dossier concerné
 *
 * @return int Taille du dossier ou du fichier en octets
 */
function mes_fichiers_lire_taille($chemin) {

	return is_dir($chemin)
		? dossier_calculer_taille($chemin)
		: filesize($chemin);
}

/**
 * Renvoie un chemin de dossier le plus propre possible en gérant le cas d'un SPIP mutualisé ou non.
 * 
 * @uses joli_repertoire()
 *
 * @param string $dossier Chemin du dossier à afficher d'une manière la plus lisible.
 */
function mes_fichiers_afficher_dossier($dossier) {

	if (
		defined('_DIR_SITE')
		and preg_match(',' . _DIR_SITE . ',', $dossier)
	) {
		$joli_repertoire = str_replace(_DIR_SITE, '', $dossier);
	} else {
		include_spip('inc/utils');
		$joli_repertoire = joli_repertoire($dossier);
	}

	return $joli_repertoire;
}

// ----------------------------

/**
 * Renvoie le fichier le plus récent d'un dossier donné.
 * Le dossier ne doit contenir que des fichiers concernant le même sujet.
 *
 * @param $dossier
 *
 * @return string
 */
function dossier_trouver_fichier_recent($dossier) {

	// Par défaut, si le dossier est vide on renvoie un nom de fichier vide
	$fichier_recent = '';

	// On considère que dans le dossier tous les fichiers concernent le même sujet, il est donc inutile
	// d'utiliser un pattern pour le fichier.
	include_spip('inc/flock');
	$fichiers = preg_files($dossier);
	$mtime_recent = 0;
	foreach ($fichiers as $_fichier) {
		if (($mtime = filemtime($_fichier)) > $mtime_recent) {
			$fichier_recent = $_fichier;
			$mtime_recent = $mtime;
		}
	}

	return $fichier_recent;
}


/**
 * Calculer la taille d'un répertoire en itérant sur son contenu.
 *
 * @link http://aidanlister.com/repos/v/function.dirsize.php
 *
 * @param string $path Chemin vers le dossier concerné
 *
 * @return int
 */
function dossier_calculer_taille($path) {

	// Par défaut, erreur ou pas, le dossier est vide
	$size = 0;

	// Trailing slash
	if (substr($path, -1, 1) !== DIRECTORY_SEPARATOR) {
		$path .= DIRECTORY_SEPARATOR;
	}

	// Sanity check
	if (is_file($path)) {
		$size = filesize($path);
	} elseif (is_dir($path)) {
		// Iterate queue
		$queue = array($path);
		for ($i = 0, $j = count($queue); $i < $j; ++$i) {
			// Open directory
			if (is_dir($queue[$i]) && $dir = @dir($queue[$i])) {
				$subdirs = array();
				while (false !== ($entry = $dir->read())) {
					// Skip pointers
					if ($entry == '.' || $entry == '..') {
						continue;
					}

					// Get list of directories or filesizes
					$path = $queue[$i] . $entry;
					if (is_dir($path)) {
						$path .= DIRECTORY_SEPARATOR;
						$subdirs[] = $path;
					} elseif (is_file($path)) {
						$size += filesize($path);
					}
				}

				// Add subdirectories to start of queue
				unset($queue[0]);
				$queue = array_merge($subdirs, $queue);

				// Recalculate stack size
				$i = -1;
				$j = count($queue);

				// Clean up
				$dir->close();
				unset($dir);
			}
		}
	}

	return $size;
}


/**
 * Convertit en octets une taille exprimée en multiple d'octets, en considérant le système d'unité utilisé, à savoir,
 * le système international en base 10 ou le système binaire.
 *
 * @param int    $taille  Taille dans l'unité spécifiée en second argument
 * @param string $unite   Abbréviation de l'unité, à savoir, `K`, `M` (défaut), `G` ou `T`.
 * @param string $systeme Système d'unité dans lequel calculer et afficher la taille lisble. Vaut `BI` (défaut) ou `SI`.
 *
 * @return int
 */
function taille_convertir_en_octets($taille, $unite = 'M', $systeme = 'BI') {

	static $puissances = [
		'k' => 1,
		'm' => 2,
		'g' => 3,
		't' => 4,
	];
	static $bases = [
		'si' => 1000,
		'bi' => 1024
	];

	// Si l'unité est incorrecte on renvoie 0
	$taille_octets = 0;
	if (
		($unite = strtolower($unite))
		and array_key_exists($unite, $puissances)
		and ($systeme = strtolower($systeme))
		and array_key_exists($systeme, $bases)
	) {
		return (int)$taille * pow($bases[$systeme], $puissances[$unite]);
	}

	return $taille_octets;
}
