<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_MES_FICHIERS_DOSSIER')) {
	/**
	 * Repertoire de stockage des archives.
	 */
	define('_MES_FICHIERS_DOSSIER', _DIR_TMP . 'mes_fichiers/');
}

/**
 * Sauvegarde des fichiers et des dossiers de personnalisation du site.
 *
 * @api
 *
 * @param null|array $liste   Liste des dossiers ou fichiers à archiver ou null pour archiver tout ce qui est possible
 * @param array      $options Options d'archivage. Pour l'instant uniquement l'auteur ayant déclenché l'archivage.
 *
 * @return string Message d'erreur éventuel ou chaine vide sinon.
 */
function archive_mf_creer($liste = null, $options = []) {
	// Message d'erreur en retour de la fonction : chaine si pas d'erreur
	$erreur = '';

	// Si on a passé null pour la liste des fichiers, c'est que l'on veut sauvegarder tout ce qui est possible
	// En effet, ce peut être un array vide dans le cas d'un problème de config.
	include_spip('inc/mes_fichiers');
	if (null === $liste) {
		// Les répertoires et fichiers qui excèdent la taille maximale configurée sont exclus
		$liste = mes_fichiers_lister(true);
	}

	// Identification de l'auteur ayant déclenché l'archivage
	$auteur = !empty($options['auteur'])
		? $options['auteur']
		: $GLOBALS['visiteur_session']['id_auteur'];

	// Si il y a quelque chose à archiver
	if (count($liste) > 0) {
		// On stocke les fichiers et répertoires source pour insérer cette liste en commentaire de l'archive
		$liste_finale = [];
		foreach ($liste as $_chemin) {
			$liste_finale[] = mes_fichiers_afficher_dossier($_chemin);
		}

		// On construit le nom de l'archive et on crée le répertoire d'accueil si il n'existe pas déjà.
		$dir = sous_repertoire(_MES_FICHIERS_DOSSIER);
		$prefixe = lire_config('mes_fichiers/prefixe', 'mf3');
		$chemin_archive = $dir . $prefixe . '_' . date('Ymd_His') . '.zip';

		// On prépare le commentaire
		if ($auteur === 'cron') {
			$auteur = 'SPIP';
		}
		$meta = [
			'auteur'  => $auteur,
			'contenu' => $liste_finale
		];

		// On lance l'archivage du contenu.
		include_spip('inc/archives');
		$archive = new Spip\Archives\SpipArchives($chemin_archive);
		if (!$archive->emballer($liste)) {
			// -- on renvoie le message d'erreur provenant de la librairie d'archivage
			$erreur = $archive->message();
		} else {
			// -- si pas d'erreur, on ajoute le commentaire
			$archive->commenter(serialize($meta));
		}
	} else {
		$erreur = _T('mes_fichiers:erreur_aucun_fichier_sauver');
	}

	// Un pipeline post_sauvegarde pour que d'autres plugins puissent agir à ce moment là.
	pipeline(
		'post_sauvegarde',
		[
			'args' => [
				'err'     => $erreur,
				'auteur'  => $auteur,
				'type'    => 'mes_fichiers_sauver',
				'archive' => $chemin_archive ?? ''
			],
			'data' => ''
		]
	);

	// Notifications si nécessaire.
	if ($notifications = charger_fonction('notifications', 'inc')) {
		$notifications(
			'archive_mf_creer',
			'',
			[
				'auteur' => $auteur,
				'err'    => $erreur]
		);
	}

	return $erreur;
}

/**
 * Supprime les fichiers d'archive considérés comme obsolètes.
 * La fonction retourne la liste des archives effectivement supprimées.
 *
 * @api
 *
 * @param array $options
 *
 * @return array Liste des archives effectivement supprimées.
 */
function archive_mf_nettoyer($options = []) {
	// Initialiser la liste des archives supprimées fournie en retour
	$liste = [];

	// Supprimer les archives dont l'échéance de garde est échue
	include_spip('inc/config');
	$duree_sauvegarde = (int) (lire_config('mes_fichiers/duree_sauvegarde', 15));
	if ($duree_sauvegarde > 0) {
		// Stocker la date actuelle pour calculer l'obsolescence
		$temps = time();

		// Identification du préfixe des archives
		$prefixe = lire_config('mes_fichiers/prefixe', 'mf3') . '_';

		// Scan des archives existantes et suppression des obsolètes
		include_spip('inc/flock');
		$archives = preg_files(_MES_FICHIERS_DOSSIER, "{$prefixe}.+[.](zip)$");
		$liste = [];
		foreach ($archives as $_archive) {
			$date_fichier = filemtime($_archive);
			if ($temps > ($date_fichier + $duree_sauvegarde * 3600 * 24)) {
				supprimer_fichier($_archive);
				$liste[] = $_archive;
			}
		}
	}

	// Identification de l'auteur ayant déclenché l'archivage
	$auteur = $options['auteur'] ?: $GLOBALS['visiteur_session']['id_auteur'];
	if ($auteur == 'cron') {
		$auteur = 'SPIP';
	}

	// Pipeline de post sauvegarde si besoin
	pipeline(
		'post_sauvegarde',
		[
			'args' => [
				'auteur' => $auteur,
				'type'   => 'mes_fichiers_cleaner',
				'liste'  => $liste,
			],
			'data' => ''
		]
	);

	// Notification de l'action
	if ($notifications = charger_fonction('notifications', 'inc')) {
		$notifications(
			'archive_mf_nettoyer',
			'',
			['liste' => $liste, 'auteur' => $auteur]
		);
	}

	return $liste;
}

/**
 * Renvoie la liste des archives disponibles au telechargement par date inverse.
 *
 * @api
 *
 * @return array
 */
function archive_mf_lister() {
	include_spip('inc/config');
	$prefixe = lire_config('mes_fichiers/prefixe', 'mf3');
	$laver_auto = (lire_config('mes_fichiers/nettoyage_journalier', 'oui') == 'oui');

	$pattern = "{$prefixe}.*\.zip$";

	include_spip('inc/flock');
	if ($laver_auto) {
		$liste = preg_files(_MES_FICHIERS_DOSSIER, $pattern);
	} else {
		$liste = preg_files(_MES_FICHIERS_DOSSIER, $pattern, 50);
	}

	// On filtre les fichiers vides ou corrompues qui sont des résultats d'erreur lors de l'archivage
	foreach ($liste as $_cle => $_archive) {
		if (!is_file($_archive)
		or !is_readable($_archive)
		or (filesize($_archive) === 0)) {
			unset($liste[$_cle]);
		}
	}

	return array_reverse($liste);
}

/**
 * Renvoie les informations sur le contenu de l'archive.
 *
 * @api
 *
 * @param string $chemin_archive Chemin de l'archive
 *
 * @return string
 */
function archive_mf_informer($chemin_archive) {
	// Extraction des informations sur l'archive
	include_spip('inc/archives');
	$archive = new Spip\Archives\SpipArchives($chemin_archive);
	$informations = $archive->informer();

	$resume = '';
	if (empty($informations['commentaire'])) {
		$resume .= _T('mes_fichiers:message_zip_propriete_nok');
		spip_log('Commentaire de l\'archive absente ou d\'un format obsolète', 'mes_fichiers' . _LOG_ERREUR);
	} else {
		$commentaire = unserialize($informations['commentaire']);

		if (
			!empty($commentaire['contenu'])
			and !empty($commentaire['auteur'])
		) {
			$liste = $commentaire['contenu'];
			$id_auteur = $commentaire['auteur'];

			// On gere le cas où l'auteur est le cron
			if ((int) $id_auteur) {
				$auteur = sql_getfetsel('nom', 'spip_auteurs', 'id_auteur=' . (int) $id_auteur);
			} else {
				$auteur = $id_auteur;
			}
			$resume .= _T('mes_fichiers:resume_zip_auteur') . ' : ' . $auteur . '<br />';
			$resume .= _T('mes_fichiers:resume_zip_compteur') . ' : ' . count($informations['fichiers']) . '<br />';
			$resume .= _T('mes_fichiers:resume_zip_contenu') . ' : ' . '<br />';
			$resume .= '<ul class="spip">';
			foreach ($liste as $_fichier) {
				$resume .= '<li>' . $_fichier . '</li>';
			}
			$resume .= '</ul>';
		}
	}

	return $resume;
}
