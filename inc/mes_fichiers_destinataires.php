<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
/**
 * @param $quoi
 * @param $id
 * @param $options
 *
 * @return null|mixed
 */
function inc_mes_fichiers_destinataires_dist($quoi, $id, $options) {

	// Recuperation des destinataires configurés
	include_spip('inc/config');
	$mails_configures = lire_config('mes_fichiers/notif_mail', '');
	$mails_configures = $mails_configures
		? explode(',', $mails_configures)
		: [];

	// On ajoute systématiquement le webmestre
	if (!empty($GLOBALS['meta']['email_webmaster'])) {
		$mails_configures[] = $GLOBALS['meta']['email_webmaster'];
	}

	// On ajoute les éventuels destinataires supplémentaires fournis par des plugins
	$destinataires = pipeline(
		'notifications_destinataires',
		[
			'args'=> [
				'quoi'   => $quoi,
				'id'     => $id,
				'options'=> $options
			],
			'data'=> $mails_configures
		]
	);

	// Nettoyage de la liste d'emails en vérifiant les doublons
	// et la validité des emails
	include_spip('inc/notifications');
	notifications_nettoyer_emails($destinataires);

	return $destinataires;
}
